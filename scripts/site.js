/**
 * @file JavaScript for Speed Reader.
 * @author Jessie Rambeloson
 * @author Rosemary Canon
 * A speed reader application that displays one word after another
 * with an enhanced character in the middle of the word
 * at a number of words inserted by the user/minute.
 * The words are fetched from a website,
 * and the speed is saved in the local storage.
 * We use an interval to continuously display the words at
 * a constant speed.
 */

'use strict';
document.addEventListener("DOMContentLoaded", setup);

//global array that will contain all the global variables
let global = {};

/**
 * @function setup initializes all the global variables
 * creates event listeners
 * @author Jessie Rambeloson
 * @author Rosemary Canon
 */
function setup(){
    //span elements
    global.before = document.querySelector("#before_focus");
    global.focus = document.querySelector("#focus");
    global.after = document.querySelector("#after_focus");
    
    //creating an event listener for the form
    global.form = document.querySelector("form");
    global.form.addEventListener("submit", start);
    
    //get button
    global.button = document.querySelector("#button");

    //the interval that is used when the play button is clicked
    global.interval;

    //keeps track of the index during iteration of array containing the words to display
    global.index = 0;

    //get wpm input element
    global.WPMinput = document.querySelector("#textbox");

    //load from local storage if a previous wpm was used
    loadExistingWPM();

    //everytime the wpm input is changed it will be saved into localstorage
    global.WPMinput.addEventListener("change", inputChange);
}

/**
 * Loads an existing WPM value if there was one that was previously stored.
 * If there is none, the value with be set to 100.
 * @author Rosemary Canon
 */
function loadExistingWPM() {
  let json = localStorage.getItem("wpm");
  if (json) {
    //display the stored wpm in the textbox
    global.WPMinput.value = JSON.parse(json);
    //save the stored wpm in the global variable WPMvalue
    global.WPMvalue = global.WPMinput.value;
  }
  else {
    //if no stored wpm exists, set the WPM value to 100
    global.WPMvalue = 100;
  }
}

/**
 * This function is the callback funtion for the form's submit event
 * @param e is the event on which the method was invoked
 * determines whether the button should be set to start or stop
 * calls @function getNextQuote to fetch the quotes
 * @author Jessie Rambeloson
 */
function start(e){
    e.preventDefault();

    //change the button to stop when it is on play stat
    //calls getNextQuote if button is in play stat
    if (global.button.getAttribute('name') == 'play'){
        global.button.setAttribute("name", "stop");
        global.button.style.background = "url('images/stop.png') no-repeat";
        global.index = 0;
        getNextQuote();
    }
    //change the button shape to play when button is in stop stat
    else{
        global.button.setAttribute("name", "play");
        global.button.style.background = "url('images/play.png') no-repeat";
        clearInterval(global.interval);
    }
}

/**
 * This function will save the WPM that is given by the user. The WPM will only be stored if
 * it satisfies certain criterias: the value has to be between 50 and 1000 (inclusive) and
 * has to be a multiple of 50. If the condition is satisfied, the value is then converted into
 * JSON and stored into local storage.
 * @author Rosemary Canon
 */
function inputChange() {
  //Get the value of the textbox
  let WPMvalue = global.WPMinput.value;
  //We have to make sure that the wpm value is only stored if the satisfies the following condition.
  if (WPMvalue >= 50 && WPMvalue <= 1000 && WPMvalue % 50 == 0) {
    let wpmJSON = JSON.stringify(WPMvalue);
    localStorage.setItem("wpm", wpmJSON);
    //stores the wpm if it is changed
    global.WPMvalue = global.WPMinput.value;
  }
}

/**
 * This function fetches the quotes from website
 * displays a message in console if there was an error
 * calls @function displayQuote when fetch is successful to initialize interval
 * @author Jessie Rambeloson
 * @author Rosemary Canon
 */
function getNextQuote(){
    fetch("https://ron-swanson-quotes.herokuapp.com/v2/quotes")
        .then (response => {
            if (!response.ok){
              throw new Error("Status code: " + reponse.status);
            }
            return response.json();
        })
        .then (json => {
            const words = json[0].split(' ');
            return words;
        })
        .then (words => displayQuote(words))
        .catch(error => console.log("An error has occurred." + error));
}

/**
 * This function initializes the interval with speedBox value
 * @param words is the array containing the words returned from fetching
 * uses @function displayWords as callback to display the words on the screen
 * @author Jessie Rambeloson
 */
function displayQuote(words){
    const msec = 60000/global.WPMinput.value;
    global.interval = setInterval(displayWords, msec, words);
}

/**
 * This function breaks the string into 3 parts and displays them on the screen
 * @param words is the array of words that will be displayed
 * @author Jessie Rambeloson
 */
function displayWords(words){
    //if index has reached end of the array and button is still on play stat,
    //reset index to 0 and call for a new quote
    if (global.index == words.length){
        clearInterval(global.interval);
        global.index = 0;
        if (global.button.getAttribute('name') != 'play'){
            getNextQuote();
        }
    }
    //choose the focus words depending on the word's length
    else{
        if (words[global.index].length == 1){
            words[global.index] = '    ' + words[global.index];
        }
        else if (words[global.index].length >= 2 && words[global.index].length <= 5){
            words[global.index] = '   ' + words[global.index];
        }
        else if (words[global.index].length > 5 && words[global.index].length <= 9){
            words[global.index] = '  ' + words[global.index];
        }
        else if (words[global.index].length > 9 && words[global.index].length <= 13){
            words[global.index] = ' ' + words[global.index];
        }

        global.before.textContent = words[global.index].substring(0, 4);
        global.focus.textContent = words[global.index].substring(4, 5);
        global.after.textContent = words[global.index].substring(5);
        global.index++;
    }
}
